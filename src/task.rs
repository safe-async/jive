use crate::runtime::RuntimeHandle;
use std::future::Future;

pub mod sync {
    pub use jitterbug::{AbortHandle, JoinError, JoinHandle};
}

pub mod unsync {
    pub use bachata::{JoinError, JoinHandle};
}

pub fn spawn<T: Send + 'static>(
    future: impl Future<Output = T> + Send + 'static,
) -> sync::JoinHandle<T> {
    RuntimeHandle::current().spawn(future)
}

pub fn spawn_local_unsend<T>(future: impl Future<Output = T> + 'static) -> unsync::JoinHandle<T> {
    RuntimeHandle::current().spawn_local_unsend(future)
}

pub fn spawn_local<T: Send + 'static>(
    future: impl Future<Output = T> + 'static,
) -> sync::JoinHandle<T> {
    RuntimeHandle::current().spawn_local(future)
}

pub fn spawn_blocking<T: Send + 'static>(
    callback: impl FnOnce() -> T + Send + 'static,
) -> sync::JoinHandle<T> {
    RuntimeHandle::current().spawn_blocking(callback)
}
