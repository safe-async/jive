use std::time::Duration;

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    jive::block_on(async move {
        println!("hewwo");

        let handles = (1..=50)
            .map(|i| {
                jive::spawn_blocking(move || {
                    std::thread::sleep(Duration::from_secs(2));
                    println!("{} blocked", i);
                })
            })
            .collect::<Vec<_>>();

        for handle in handles {
            handle.await?;
        }

        Ok(())
    })
}
