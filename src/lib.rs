pub use foxtrot::{io, net, time};

pub mod sync {
    pub use jitterbug::{oneshot, Dropped as OneshotError, Receiver, Sender};
}

pub mod runtime;
pub mod task;

pub use task::{spawn, spawn_blocking, spawn_local, spawn_local_unsend};

pub fn block_on<T>(future: impl std::future::Future<Output = T>) -> T {
    runtime::Runtime::new().block_on(future)
}
