use std::time::Duration;

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    jive::block_on(async {
        println!("hewwo");

        let handles = (0..10)
            .map(|i| {
                jive::spawn(async move {
                    let handles = (1..=10)
                        .map(|j| {
                            jive::spawn_local(async move {
                                jive::time::sleep(Duration::from_secs(2)).await;
                                println!("{} slept", i * 10 + j);
                            })
                        })
                        .collect::<Vec<_>>();

                    for handle in handles {
                        handle.await?;
                    }

                    println!("{i} joined");

                    Ok(()) as Result<_, jive::task::sync::JoinError>
                })
            })
            .collect::<Vec<_>>();

        for handle in handles {
            handle.await??;
        }

        println!("all joined");

        Ok(())
    })
}
