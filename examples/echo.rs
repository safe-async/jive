use jive::{
    io::{Async, Nonblocking, Readiness},
    net::TcpListener,
};
use read_write_buf::ReadWriteBuf;
use std::time::Duration;

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    jive::block_on(async move {
        jive::spawn(async move {
            let mut listener = match Async::<TcpListener>::bind(([127, 0, 0, 1], 3456)).await {
                Ok(listener) => listener,
                Err(_) => return,
            };

            println!("Listening on port 3456");

            loop {
                let (mut stream, _addr) = match listener.accept().await {
                    Ok(tup) => tup,
                    Err(_) => return,
                };

                jive::spawn(async move {
                    println!("Accepted stream");

                    let mut ring_buf = ReadWriteBuf::<10>::new();
                    let mut interests = Readiness::read();

                    'l2: loop {
                        let readiness = stream.ready(interests).await?;

                        if readiness.is_hangup() {
                            break;
                        }

                        if readiness.is_read() {
                            while let Some(buf) = ring_buf.for_reading() {
                                match stream.read_nonblocking(buf)? {
                                    Nonblocking::Ready(n) if n > 0 => {
                                        let should_break = n < buf.len();
                                        ring_buf.advance_read(n);
                                        if should_break {
                                            break;
                                        }
                                    }
                                    Nonblocking::Ready(_) if ring_buf.is_empty() => break 'l2,
                                    Nonblocking::Ready(_) | Nonblocking::WouldBlock => break,
                                }
                            }

                            if !ring_buf.is_empty() {
                                interests = Readiness::read() | Readiness::write();
                            }
                        }

                        if readiness.is_write() {
                            while let Some(buf) = ring_buf.for_writing() {
                                match stream.write_nonblocking(buf)? {
                                    Nonblocking::Ready(n) => {
                                        ring_buf.advance_write(n);
                                        if ring_buf.is_empty() {
                                            interests = Readiness::read();
                                        }
                                    }
                                    Nonblocking::WouldBlock => break,
                                }
                            }
                        }
                    }

                    println!("Stream closed");

                    Ok(()) as jive::io::Result<()>
                });
            }
        });

        jive::time::sleep(Duration::from_secs(60 * 2)).await;
        println!("Stopping");
        Ok(())
    })
}
